#[derive(Clone)]
struct NBTreeNode<K, V> {
    k: K,
    v: V,
    l: NBTreeMap<K, V>,
    r: NBTreeMap<K, V>,
}

#[derive(Clone)]
pub struct NBTreeMap<K, V> {
    node: Option<Box<NBTreeNode<K, V>>>,
}

impl<K, V> NBTreeMap<K, V> {
    pub fn new() -> Self {
        NBTreeMap { node: None }
    }
    pub fn len(&self) -> usize {
        match &self.node {
            &None => {
                0
            } &Some(ref node) => {
                1 + node.l.len() + node.r.len()
            }
        }
    }
    pub fn extend_unto<'a>(&'a self, v: &mut Vec<(&'a K, &'a V)>) {
        match &self.node {
            &None => {
            } &Some(ref node) => {
                node.l.extend_unto(v);
                v.push((&node.k, &node.v));
                node.r.extend_unto(v);
            }
        }
    }
    pub fn extend_unto_mut<'a>(&'a mut self, v: &mut Vec<(&'a K, &'a mut V)>) {
        match &mut self.node {
            &mut None => {
            } &mut Some(ref mut node) => {
                node.l.extend_unto_mut(v);
                v.push((&node.k, &mut node.v));
                node.r.extend_unto_mut(v);
            }
        }
    }
    pub fn to_vec(&self) -> Vec<(&K, &V)> {
        let mut v = vec![];
        self.extend_unto(&mut v);
        v
    }
    pub fn to_mut_vec(&mut self) -> Vec<(&K, &mut V)> {
        let mut v = vec![];
        self.extend_unto_mut(&mut v);
        v
    }
}

impl<K, V> NBTreeMap<K, V> where K: PartialOrd {
    pub fn insert<'a>(&mut self, k: K, v: V) -> &mut V {
        let mut root = &mut self.node;
        loop {
            match root {
                &mut None => {
                    *root = Some(Box::new(NBTreeNode{ k, v, l: NBTreeMap::new(), r: NBTreeMap::new() }));
                    return &mut root.as_mut().unwrap().v;
                } &mut Some(ref mut node) => {
                    if k == node.k {
                        return &mut node.v;
                    } else if k < node.k {
                        root = &mut node.l.node;
                    } else {
                        root = &mut node.r.node;
                    }
                }
            }
        }
    }
    pub fn update(&mut self, k: K, v: V) {
        let mut root = &mut self.node;
        loop {
            match root {
                &mut None => {
                    *root = Some(Box::new(NBTreeNode{ k, v, l: NBTreeMap::new(), r: NBTreeMap::new() }));
                    return;
                } &mut Some(ref mut node) => {
                    if k == node.k {
                        node.k = k;
                        node.v = v;
                        return;
                    } else if k < node.k {
                        root = &mut node.l.node;
                    } else {
                        root = &mut node.r.node;
                    }
                }
            }
        }
    }
    pub fn lookup(&self, k: &K) -> Option<&V> {
        let mut root = &self.node;
        loop {
            match root {
                &None => {
                    return None;
                } &Some(ref node) => {
                    if *k == node.k {
                        return Some(&node.v);
                    } else if *k < node.k {
                        root = &node.l.node;
                    } else {
                        root = &node.r.node;
                    }
                }
            }
        }
    }
    pub fn lookup_mut(&mut self, k: &K) -> Option<&mut V> {
        let mut root = &mut self.node;
        loop {
            match root {
                &mut None => {
                    return None;
                } &mut Some(ref mut node) => {
                    if *k == node.k {
                        return Some(&mut node.v);
                    } else if *k < node.k {
                        root = &mut node.l.node;
                    } else {
                        root = &mut node.r.node;
                    }
                }
            }
        }
    }
}
