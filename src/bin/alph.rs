#[macro_use]
extern crate lazy_static;
extern crate regex;
extern crate num;

extern crate nbtree;

use std::io::prelude::*;
use std::io;
use std::io::BufReader;
use std::fs::File;
use std::fmt::Display;

use num::traits::*;

use regex::Regex;

use nbtree::NBTreeMap;

lazy_static! {
    static ref WORDSPLIT: Regex = Regex::new(r"\W+").unwrap();
}

fn capitalize(word: &str) -> String {
    let mut chars = word.chars();
    let mut s = chars.next().map(|c| c.to_uppercase().to_string()).unwrap_or_else(String::new);
    s.extend(chars);
    s
}

fn build_table<N: Num + Clone>(r: &mut BufRead) -> io::Result<NBTreeMap<String, N>> {
    let mut m = NBTreeMap::new();
    for line in r.lines() {
        let line = line?;
        for word in WORDSPLIT.split(&line) {
            let word = capitalize(word);
            let n = m.insert(word, N::zero());
            *n = n.clone() + N::one();
        }
    }
    Ok(m)
}

fn report<N: Ord + Display>(w: &mut Write, m: &NBTreeMap<String, N>) -> io::Result<()> {
    let mut pairs = m.to_vec();
    pairs.sort_by(|a, b| a.1.cmp(b.1));
    for pair in pairs {
        writeln!(w, "{:>24}: {:>10}", pair.0, pair.1)?;
    }
    Ok(())
}

fn main() {
    for i in 1..101 {
        println!("# Begin Iteration {}", i);
        let m = {
            let mut f = BufReader::new(File::open("TEXT-PCE-127.txt").unwrap());
            build_table::<i64>(&mut f).unwrap()
        };
        report(&mut io::stdout().lock(), &m).unwrap();
        println!("# End Iteration {}", i);
    }
}
